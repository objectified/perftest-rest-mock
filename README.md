# Performance Test Mock Service

## Introduction

This is a REST service that behaves as you want, for use during testing or experimenting.

## How to install

The easiest way is to use docker:

```
    docker run -ti --rm -p 8000:8000 objectified/perftest-rest-mock:latest
```

If you want to run it without Docker, check out the Dockerfile, which contains exact information on how it is set up.


## How to use

Several endpoints:

```
    http://localhost:8000/service/  - a basic healthcheck
    http://localhost:8000/service/fixed-delay  - responds after the configured delay 
    http://localhost:8000/service/random-delay - delays response using a delay between the configured start_delay/end_delay values 
    http://localhost:8000/service/setconfig  - sets a config value for the specified key:
        http://localhost:8000/service/setconfig/delay/3  # sets fixed delay to 3 seconds
        http://localhost:8000/service/setconfig/start_delay/3  # sets start delay for random delays to 3 seconds
        http://localhost:8000/service/setconfig/end_delay/3  # sets end delay for random delays to 3 seconds
    http://localhost:8000/service/backend-timeout - tries to connect to a non-routable address, and writes an error in the log when it timed out (info.log)
```
