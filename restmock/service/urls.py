from django.urls import path
from service import views

urlpatterns = [
    path('', views.index),
    path('fixed-delay', views.fixed_delay),
    path('random-delay', views.random_delay),
    path('backend-timeout', views.force_connect_timeout),
    path('setconfig/<str:key>/<str:value>', views.setconfig)
]
