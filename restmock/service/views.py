import time
import logging
import random

from django.shortcuts import render
from django.http import HttpResponse

import requests

from .models import RuntimeConfig


logger = logging.getLogger(__name__)


def index(request):
    return HttpResponse('I am alive')


def setconfig(request, key, value):
    try:
        config_record_results = RuntimeConfig.objects.filter(key=key)
        if len(config_record_results) < 1:
            raise Exception('config key not found')

        config_record = config_record_results[0]
        config_record.value = value
        config_record.save()

        return HttpResponse('Succesfully set {} to {}'.format(key, value))
    except Exception as e:
        return HttpResponse('Failed to set message: {}'.format(e))


def fixed_delay(request):
    delay_time_record = RuntimeConfig.objects.filter(key='delay')[0]
    delay_time = int(delay_time_record.value)

    time.sleep(int(delay_time))

    logger.info('Elapsed time: {} seconds'.format(delay_time))

    return HttpResponse('I am here after {} seconds'.format(delay_time))


def random_delay(request):
    start_delay_record = RuntimeConfig.objects.filter(key='start_delay')[0]
    start_delay = int(start_delay_record.value)

    end_delay_record = RuntimeConfig.objects.filter(key='end_delay')[0]
    end_delay = int(end_delay_record.value)

    delay = random.randint(start_delay, end_delay)

    time.sleep(delay)

    logger.info('Elapsed time: {} seconds'.format(delay))

    return HttpResponse('I am here after {} seconds'.format(delay))


def force_connect_timeout(request):
    timeout = 10
    try:
        requests.get('http://192.0.2.1', timeout=timeout)
        logger.error('Request unexpectedly succeeded, should not be routable')
        return HttpResponse('Request succeeded, but should not succeed', status=500)
    except:
        err = 'Could not connect to http://192.0.2.1 within {} seconds'.format(timeout)
        logger.error(err)
        return HttpResponse(err, status=504)
